## Auto Build Repository

This repository contains all reference to dockerfile builds. Dockerfiles are used to build images automatically by 
reading the instructions from a Dockerfile. It contains all the commands a user could call on the command line to assemble
an image. 

Lastly, this repo works in conjunction with Docker Hub to automate the image build process. Docker Hub can automatically 
build images from source code and automatically push the built image to your Docker repositories. 
